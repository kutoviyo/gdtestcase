package com.okutovyi.gdtestcase.presenter

import com.okutovyi.gdtestcase.data.entity.Info
import com.okutovyi.gdtestcase.data.entity.WeatherInfo
import com.okutovyi.gdtestcase.repository.ForecastRepository
import com.okutovyi.gdtestcase.ui.weatherdetails.WeatherDetailsContract
import com.okutovyi.gdtestcase.ui.weatherdetails.WeatherDetailsPresenter
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class WeatherDetailsPresenterTest {

    @Mock
    lateinit var forecastRepo: ForecastRepository

    @Mock
    lateinit var view: WeatherDetailsContract.View

    lateinit var presenter: WeatherDetailsPresenter
    private val city = "City"

    @Before
    fun setup() {
        presenter = WeatherDetailsPresenter(forecastRepo)
    }

    @Test
    fun emptyListTest() {
        Mockito.`when`(forecastRepo.cachedWeatherInfo).thenReturn(null)
        presenter.attachView(view)

        Mockito.verify(view).showListIsNullError()
    }

    @Test
    fun defaultCaseTest() {
        Mockito.`when`(forecastRepo.cachedWeatherInfo).thenReturn(getElements())
        presenter.attachView(view)
        presenter.loadDataForItem(2)

        Mockito.verify(view).showInfo(Info(200L, 22.0, 18.0, "Title2", "Desc"))
    }

    private fun getElements(): WeatherInfo {
        return WeatherInfo(
            "City", listOf(
                Info(100L, 20.0, 18.0, "Title", "Desc"),
                Info(120L, 21.0, 18.0, "Title1", "Desc"),
                Info(200L, 22.0, 18.0, "Title2", "Desc"),
                Info(30L, 23.0, 18.0, "Title3", "Desc")
            )
        )
    }
}