package com.okutovyi.gdtestcase.presenter

import com.okutovyi.gdtestcase.R
import com.okutovyi.gdtestcase.repository.ForecastRepository
import com.okutovyi.gdtestcase.ui.search.SearchContract
import com.okutovyi.gdtestcase.ui.search.SearchPresenter
import io.reactivex.rxjava3.core.Completable
import org.junit.Before
import org.junit.ClassRule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class SearchPresenterTest {

    companion object {
        @ClassRule
        @JvmField
        val schedulers = RxImmediateSchedulerRule()
    }

    @Mock
    lateinit var forecastRepo: ForecastRepository

    @Mock
    lateinit var view: SearchContract.View

    lateinit var presenter: SearchPresenter
    private val city = "City"

    @Before
    fun setup() {
        Mockito.`when`(forecastRepo.loadWeatherForecast(city)).thenReturn(Completable.complete())
        presenter = SearchPresenter(forecastRepo)
        presenter.attachView(view)
    }

    @Test
    fun testEmptySearch() {
        presenter.onSearchCalled("")
        Mockito.verify(view).showValidationError(R.string.search_field_must_not_be_null)
    }

    @Test
    fun testSearch() {
        presenter.onSearchCalled(city)
        Mockito.verify(view).showProgress()
        Mockito.verify(forecastRepo).loadWeatherForecast(city)
    }
}