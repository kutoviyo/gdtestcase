package com.okutovyi.gdtestcase.presenter

import com.okutovyi.gdtestcase.data.entity.Info
import com.okutovyi.gdtestcase.data.entity.WeatherInfo
import com.okutovyi.gdtestcase.repository.ForecastRepository
import com.okutovyi.gdtestcase.ui.weatherlist.WeatherListContract
import com.okutovyi.gdtestcase.ui.weatherlist.WeatherListPresenter
import com.okutovyi.gdtestcase.ui.weatherlist.WeatherViewModel
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class WeatherListPresenterTest {

    @Mock
    lateinit var forecastRepo: ForecastRepository

    @Mock
    lateinit var view: WeatherListContract.View

    lateinit var presenter: WeatherListPresenter
    private val city = "City"

    @Before
    fun setup() {
        presenter = WeatherListPresenter(forecastRepo)
    }

    @Test
    fun emptyListTest() {
        Mockito.`when`(forecastRepo.cachedWeatherInfo).thenReturn(null)
        presenter.attachView(view)

        Mockito.verify(view).showListIsNullError()
    }

    @Test
    fun defaultCaseTest() {
        Mockito.`when`(forecastRepo.cachedWeatherInfo).thenReturn(getElements())
        presenter.attachView(view)

        Mockito.verify(view).showCity(city)
        Mockito.verify(view).showForecast(getViewModels())
    }

    private fun getElements(): WeatherInfo {
        return WeatherInfo(
            "City", listOf(
                Info(100L, 20.0, 18.0, "Title", "Desc"),
                Info(120L, 21.0, 18.0, "Title1", "Desc"),
                Info(200L, 22.0, 18.0, "Title2", "Desc"),
                Info(30L, 23.0, 18.0, "Title3", "Desc")
            )
        )
    }

    private fun getViewModels(): List<WeatherViewModel> {
        return listOf(
            WeatherViewModel("Title", 20.0),
            WeatherViewModel("Title1", 21.0),
            WeatherViewModel("Title2", 22.0),
            WeatherViewModel("Title3", 23.0)
        )
    }
}