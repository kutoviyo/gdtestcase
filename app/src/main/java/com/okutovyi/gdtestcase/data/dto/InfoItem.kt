package com.okutovyi.gdtestcase.data.dto

import com.google.gson.annotations.SerializedName

data class InfoItem(
    @SerializedName("dt")
    val timeOfDateForecasted: Long,
    @SerializedName("main")
    val main: Main,
    @SerializedName("weather")
    val weather: List<Weather>
)
