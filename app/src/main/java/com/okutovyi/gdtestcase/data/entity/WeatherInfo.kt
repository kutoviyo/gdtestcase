package com.okutovyi.gdtestcase.data.entity

import com.okutovyi.gdtestcase.data.dto.WeatherInfoResponse

data class WeatherInfo(
    val cityName: String,
    val list: List<Info>
) {
    constructor(weatherInfoResponse: WeatherInfoResponse) : this(
        cityName = weatherInfoResponse.city.name,
        list = weatherInfoResponse.list.map { Info(it) }.toList()
    )

}
