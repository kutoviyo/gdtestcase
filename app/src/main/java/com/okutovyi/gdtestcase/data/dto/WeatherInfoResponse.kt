package com.okutovyi.gdtestcase.data.dto

import com.google.gson.annotations.SerializedName

data class WeatherInfoResponse(
    @SerializedName("cod")
    val code: Int,
    @SerializedName("city")
    val city: City,
    @SerializedName("list")
    val list: List<InfoItem>
)
