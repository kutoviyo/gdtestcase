package com.okutovyi.gdtestcase.data.entity

import com.okutovyi.gdtestcase.data.dto.InfoItem

data class Info(
    val timeOfDateForecasted: Long,
    val temp: Double,
    val feelsLike: Double,
    val weatherTitle: String,
    val weatherDescription: String
) {
    constructor(info: InfoItem) : this(
        timeOfDateForecasted = info.timeOfDateForecasted,
        temp = info.main.temp,
        feelsLike = info.main.feelsLike,
        weatherTitle = info.weather.first().main,
        weatherDescription = info.weather.first().description
    )
}
