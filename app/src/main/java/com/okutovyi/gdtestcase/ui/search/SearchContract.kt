package com.okutovyi.gdtestcase.ui.search

import com.okutovyi.gdtestcase.ui.base.MvpPresenter
import com.okutovyi.gdtestcase.ui.base.MvpView

class SearchContract {

    interface View : MvpView {
        fun showValidationError(resId: Int)
        fun showForecast()
    }

    interface Presenter : MvpPresenter<View> {
        fun onSearchCalled(searchValue: String)
    }
}