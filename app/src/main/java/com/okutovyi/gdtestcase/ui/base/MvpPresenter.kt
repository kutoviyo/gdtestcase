package com.okutovyi.gdtestcase.ui.base

interface MvpPresenter<V : MvpView> {

    fun attachView(view: V)

    fun detachView()

    fun getView(): V?
}