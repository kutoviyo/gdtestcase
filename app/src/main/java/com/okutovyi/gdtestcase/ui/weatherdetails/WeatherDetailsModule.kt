package com.okutovyi.gdtestcase.ui.weatherdetails

import com.okutovyi.gdtestcase.di.scopes.ActivityScope
import dagger.Binds
import dagger.Module

@Module
abstract class WeatherDetailsModule {

    @ActivityScope
    @Binds
    abstract fun bindWeatherDetailsPresenter(presenter: WeatherDetailsPresenter): WeatherDetailsContract.Presenter

    @ActivityScope
    @Binds
    abstract fun bindWeatherDetailsView(activity: WeatherDetailsActivity): WeatherDetailsContract.View
}