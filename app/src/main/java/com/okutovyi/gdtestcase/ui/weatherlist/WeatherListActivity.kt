package com.okutovyi.gdtestcase.ui.weatherlist

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.afollestad.materialdialogs.MaterialDialog
import com.okutovyi.gdtestcase.R
import com.okutovyi.gdtestcase.ui.base.BaseActivity
import com.okutovyi.gdtestcase.ui.weatherdetails.WeatherDetailsActivity
import kotlinx.android.synthetic.main.a_weather_list.*
import kotlinx.android.synthetic.main.v_toolbar.*
import javax.inject.Inject

class WeatherListActivity : BaseActivity(), WeatherListContract.View {

    @Inject
    lateinit var presenter: WeatherListPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.a_weather_list)
        presenter.attachView(this)

        setOnClickListeners()
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }

    override fun showCity(cityName: String) {
        tvToolbarTitle.text = cityName
    }

    override fun showDate(date: String) {
        tvToolbarDate.text = date
    }

    override fun showForecast(list: List<WeatherViewModel>) {
        rvForecast.layoutManager = LinearLayoutManager(this)
        rvForecast.adapter = WeatherListAdapter(list, listener)
    }

    override fun showDetails(pos: Int, city: String) =
        WeatherDetailsActivity.startForEdit(this, pos, city)

    override fun showListIsNullError() {
        MaterialDialog(this).show {
            title(R.string.something_went_wrong)
            message(R.string.try_to_load_data)
            positiveButton(R.string.ok) { dialog ->
                dialog.dismiss()
                onBackPressed()
            }
        }
    }

    private fun setOnClickListeners() {
        ivToolbarIcon.setOnClickListener { onBackPressed() }
    }

    private val listener = object : OnItemClickListener {
        override fun onItemClicked(pos: Int) {
            presenter.onItemClicked(pos)
        }
    }
}