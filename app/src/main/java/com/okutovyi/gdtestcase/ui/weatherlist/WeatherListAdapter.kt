package com.okutovyi.gdtestcase.ui.weatherlist

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.okutovyi.gdtestcase.R
import com.okutovyi.gdtestcase.common.inflate

class WeatherListAdapter(
    private var items: List<WeatherViewModel>,
    private val listener: OnItemClickListener
) : RecyclerView.Adapter<WeatherListAdapter.WeatherViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherViewHolder =
        WeatherViewHolder(parent.inflate(R.layout.weather_item))

    override fun onBindViewHolder(holder: WeatherViewHolder, position: Int) =
        holder.bind(position, items[position], listener)

    override fun getItemCount() = items.size

    override fun getItemId(position: Int) = position.toLong()

    class WeatherViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        private val tvTitle: TextView = view.findViewById(R.id.tvTitle)
        private val tvTemp: TextView = view.findViewById(R.id.tvTemp)

        fun bind(position: Int, weatherVM: WeatherViewModel, listener: OnItemClickListener) {
            tvTitle.text = weatherVM.title
            tvTemp.text = view.resources.getString(R.string.temperature, weatherVM.temp.toString())
            view.setOnClickListener { listener.onItemClicked(position) }
        }

    }
}

interface OnItemClickListener {
    fun onItemClicked(pos: Int)
}
