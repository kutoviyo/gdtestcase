package com.okutovyi.gdtestcase.ui.search

import android.os.Bundle
import com.okutovyi.gdtestcase.R
import com.okutovyi.gdtestcase.common.launchActivity
import com.okutovyi.gdtestcase.ui.base.BaseActivity
import com.okutovyi.gdtestcase.ui.weatherlist.WeatherListActivity
import kotlinx.android.synthetic.main.a_search.*
import javax.inject.Inject

class SearchActivity : BaseActivity(), SearchContract.View {

    @Inject
    lateinit var presenter: SearchPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.a_search)
        presenter.attachView(this)

        setOnClickListeners()
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }

    override fun showForecast() = launchActivity(WeatherListActivity::class.java)

    override fun showValidationError(resId: Int) {
        etSearchField.error = resources.getString(resId)
    }

    private fun setOnClickListeners() {
        btnSearch.setOnClickListener {
            etSearchField.error = null
            presenter.onSearchCalled(etSearchField.text.toString())
        }
    }
}