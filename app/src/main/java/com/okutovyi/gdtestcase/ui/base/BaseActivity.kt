package com.okutovyi.gdtestcase.ui.base

import android.app.AlertDialog
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import com.afollestad.materialdialogs.MaterialDialog
import com.okutovyi.gdtestcase.R
import dagger.android.support.DaggerAppCompatActivity

abstract class BaseActivity : DaggerAppCompatActivity(), MvpView {

    private lateinit var waitSpinnerDialog: Dialog

    override fun hideProgress() = waitSpinnerDialog.dismiss()

    override fun showProgress() {
        val builder = AlertDialog.Builder(this, R.style.ProgressSpinnerDialog)
        builder.setView(R.layout.v_progress)
        waitSpinnerDialog = builder.create()
        waitSpinnerDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        waitSpinnerDialog.setCancelable(false)
        waitSpinnerDialog.show()

    }

    override fun showError(message: Int) {
        val dialog = MaterialDialog(this)
            .title(R.string.error)
            .message(message)
        dialog.show()
    }
}