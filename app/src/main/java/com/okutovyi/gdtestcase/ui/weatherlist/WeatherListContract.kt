package com.okutovyi.gdtestcase.ui.weatherlist

import com.okutovyi.gdtestcase.ui.base.MvpPresenter
import com.okutovyi.gdtestcase.ui.base.MvpView

class WeatherListContract {

    interface View : MvpView {
        fun showCity(cityName: String)
        fun showDate(date: String)
        fun showForecast(list: List<WeatherViewModel>)
        fun showListIsNullError()
        fun showDetails(pos: Int, city:String)
    }

    interface Presenter : MvpPresenter<View> {
        fun onItemClicked(pos: Int)
    }
}