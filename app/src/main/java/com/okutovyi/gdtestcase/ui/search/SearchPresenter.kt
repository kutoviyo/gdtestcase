package com.okutovyi.gdtestcase.ui.search

import android.util.Log
import com.okutovyi.gdtestcase.R
import com.okutovyi.gdtestcase.repository.ForecastRepository
import com.okutovyi.gdtestcase.ui.base.BasePresenter
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

class SearchPresenter @Inject constructor(private val forecastRepository: ForecastRepository) :
    BasePresenter<SearchContract.View>(),
    SearchContract.Presenter {

    companion object {
        private val TAG = SearchPresenter::class.java.simpleName
    }

    override fun onSearchCalled(searchValue: String) {
        if (searchValue.isBlank()) {
            getView()?.showValidationError(R.string.search_field_must_not_be_null)
        } else {
            getView()?.showProgress()
            loadForecast(searchValue)
        }
    }

    private fun loadForecast(city: String) {
        addSubscription(
            forecastRepository.loadWeatherForecast(city)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    getView()?.showForecast()
                    getView()?.hideProgress()
                }, {
                    getView()?.hideProgress()
                    it.message?.let { it1 -> Log.e(TAG, it1) }
                    getView()?.showError(R.string.something_went_wrong)
                })
        )
    }
}
