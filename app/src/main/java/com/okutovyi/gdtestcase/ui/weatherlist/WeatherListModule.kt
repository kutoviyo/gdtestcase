package com.okutovyi.gdtestcase.ui.weatherlist

import com.okutovyi.gdtestcase.di.scopes.ActivityScope
import dagger.Binds
import dagger.Module

@Module
abstract class WeatherListModule {

    @ActivityScope
    @Binds
    abstract fun bindWeatherListPresenter(presenter: WeatherListPresenter): WeatherListContract.Presenter

    @ActivityScope
    @Binds
    abstract fun bindWeatherListView(activity: WeatherListActivity): WeatherListContract.View
}