package com.okutovyi.gdtestcase.ui.weatherdetails

import com.okutovyi.gdtestcase.repository.ForecastRepository
import com.okutovyi.gdtestcase.ui.base.BasePresenter
import javax.inject.Inject

class WeatherDetailsPresenter @Inject constructor(private val forecastRepository: ForecastRepository) :
    BasePresenter<WeatherDetailsContract.View>(),
    WeatherDetailsContract.Presenter {

    override fun attachView(view: WeatherDetailsContract.View) {
        super.attachView(view)
        prepareData()
    }

    override fun loadDataForItem(position: Int) {
        forecastRepository.cachedWeatherInfo?.let {
            val data = it.list[position]
            getView()?.showInfo(data)
        }
    }

    private fun prepareData() {
        getView()?.hideDate()
        if (forecastRepository.cachedWeatherInfo == null) {
            getView()?.showListIsNullError()
        }
    }

}