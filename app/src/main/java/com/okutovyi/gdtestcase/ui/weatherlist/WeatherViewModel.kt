package com.okutovyi.gdtestcase.ui.weatherlist

data class WeatherViewModel(
    val title: String,
    val temp: Double
)