package com.okutovyi.gdtestcase.ui.weatherdetails

import com.okutovyi.gdtestcase.data.entity.Info
import com.okutovyi.gdtestcase.ui.base.MvpPresenter
import com.okutovyi.gdtestcase.ui.base.MvpView

class WeatherDetailsContract {

    interface View : MvpView {
        fun showListIsNullError()
        fun showCity(cityName: String)
        fun hideDate()
        fun showInfo(data: Info)
    }

    interface Presenter : MvpPresenter<View> {
        fun loadDataForItem(position: Int)
    }
}