package com.okutovyi.gdtestcase.ui.weatherdetails

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.afollestad.materialdialogs.MaterialDialog
import com.okutovyi.gdtestcase.R
import com.okutovyi.gdtestcase.common.launchActivity
import com.okutovyi.gdtestcase.common.setVisible
import com.okutovyi.gdtestcase.data.entity.Info
import com.okutovyi.gdtestcase.ui.base.BaseActivity
import com.okutovyi.gdtestcase.ui.search.SearchActivity
import kotlinx.android.synthetic.main.a_weather_details.*
import kotlinx.android.synthetic.main.v_toolbar.*
import javax.inject.Inject

class WeatherDetailsActivity : BaseActivity(), WeatherDetailsContract.View {

    companion object {
        private const val KEY_ITEM_POS: String = "WeatherDetailsActivity.posId"
        private const val KEY_ITEM_CITY: String = "WeatherDetailsActivity.city"

        fun startForEdit(context: Context, posId: Int, city: String) = context.startActivity(
            Intent(context, WeatherDetailsActivity::class.java)
                .putExtra(KEY_ITEM_POS, posId)
                .putExtra(KEY_ITEM_CITY, city)
        )
    }

    @Inject
    lateinit var presenter: WeatherDetailsPresenter
    private val position by lazy { intent.getIntExtra(KEY_ITEM_POS, -1) }
    private val city by lazy { intent.getStringExtra(KEY_ITEM_CITY) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.a_weather_details)
        presenter.attachView(this)
    }

    override fun onResume() {
        super.onResume()
        presenter.loadDataForItem(position)
        city?.let { showCity(it) }
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }

    override fun showCity(cityName: String) {
        tvToolbarTitle.text = cityName
    }

    override fun showInfo(data: Info) {
        tvTemperature.text = data.temp.toString()
        tvFeelsLike.text = resources.getString(R.string.feels_like, data.feelsLike.toString())
        tvTitle.text = data.weatherTitle
        tvDescr.text = data.weatherDescription
    }

    override fun hideDate() = tvToolbarDate.setVisible(false)

    override fun showListIsNullError() {
        MaterialDialog(this).show {
            title(R.string.something_went_wrong)
            message(R.string.try_to_load_data)
            positiveButton(R.string.ok) { dialog ->
                dialog.dismiss()
                launchActivity(SearchActivity::class.java)
                finish()
            }
        }
    }
}