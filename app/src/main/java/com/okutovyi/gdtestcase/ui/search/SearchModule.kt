package com.okutovyi.gdtestcase.ui.search

import com.okutovyi.gdtestcase.di.scopes.ActivityScope
import dagger.Binds
import dagger.Module

@Module
abstract class SearchModule {

    @ActivityScope
    @Binds
    abstract fun bindSearchPresenter(presenter: SearchPresenter): SearchContract.Presenter

    @ActivityScope
    @Binds
    abstract fun bindSearchView(activity: SearchActivity): SearchContract.View
}