package com.okutovyi.gdtestcase.ui.base

interface MvpView {

    fun showProgress()

    fun hideProgress()

    fun showError(message: Int)
}