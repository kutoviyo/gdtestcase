package com.okutovyi.gdtestcase.ui.base

import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable


abstract class BasePresenter<V : MvpView> : MvpPresenter<V> {

    private var view: V? = null
    private val compositeDisposable = CompositeDisposable()

    override fun attachView(view: V) {
        this.view = view
    }

    override fun getView(): V? = view

    override fun detachView() {
        compositeDisposable.clear()
    }

    fun addSubscription(d: Disposable) {
        compositeDisposable.add(d)
    }
}