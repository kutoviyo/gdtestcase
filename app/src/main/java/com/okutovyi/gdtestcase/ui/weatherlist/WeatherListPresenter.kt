package com.okutovyi.gdtestcase.ui.weatherlist

import com.okutovyi.gdtestcase.data.entity.Info
import com.okutovyi.gdtestcase.repository.ForecastRepository
import com.okutovyi.gdtestcase.ui.base.BasePresenter
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class WeatherListPresenter @Inject constructor(private val forecastRepository: ForecastRepository) :
    BasePresenter<WeatherListContract.View>(),
    WeatherListContract.Presenter {

    var city: String? = null

    override fun attachView(view: WeatherListContract.View) {
        super.attachView(view)
        prepareData()
    }

    override fun onItemClicked(pos: Int) {
        city?.let { getView()?.showDetails(pos, it) }
    }

    private fun prepareData() {
        if (forecastRepository.cachedWeatherInfo == null) {
            getView()?.showListIsNullError()
        }

        forecastRepository.cachedWeatherInfo?.let { cachedData ->
            city = cachedData.cityName
            getView()?.showCity(cachedData.cityName)
            getView()?.showDate(
                SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(Date())
            )
            getView()?.showForecast(cachedData.list.map { item -> toViewModels(item) }.toList())
        }
    }

    private fun toViewModels(item: Info): WeatherViewModel =
        WeatherViewModel(title = item.weatherTitle, temp = item.temp)
}