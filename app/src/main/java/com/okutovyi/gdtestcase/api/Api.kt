package com.okutovyi.gdtestcase.api

import com.okutovyi.gdtestcase.data.dto.WeatherInfoResponse
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {

    @GET("forecast")
    fun getForecast(@Query("q") city: String): Single<WeatherInfoResponse>

}