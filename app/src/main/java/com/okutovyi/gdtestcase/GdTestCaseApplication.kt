package com.okutovyi.gdtestcase

import android.content.Context
import com.okutovyi.gdtestcase.di.AppComponent
import com.okutovyi.gdtestcase.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class GdTestCaseApplication : DaggerApplication() {

    private lateinit var appComponent: AppComponent

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        initializeInjector()
        return appComponent
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    private fun initializeInjector() {
        appComponent = DaggerAppComponent
            .builder()
            .application(this)
            .build()
//        appComponent.inject(this)
    }

    companion object {
        var instance: GdTestCaseApplication? = null
            private set

        operator fun get(context: Context): GdTestCaseApplication {
            return context.applicationContext as GdTestCaseApplication
        }
    }
}