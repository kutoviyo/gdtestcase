package com.okutovyi.gdtestcase.repository

import com.okutovyi.gdtestcase.api.Api
import com.okutovyi.gdtestcase.data.entity.WeatherInfo
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.schedulers.Schedulers

class ForecastRepository(private val api: Api) {

    var cachedWeatherInfo: WeatherInfo? = null
        private set

    fun loadWeatherForecast(city: String): Completable = api.getForecast(city)
        .subscribeOn(Schedulers.io())
        .flatMapCompletable {
            cachedWeatherInfo = WeatherInfo(it)
            Completable.complete()
        }
}