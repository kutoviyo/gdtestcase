package com.okutovyi.gdtestcase.di.scopes

import javax.inject.Scope

@Scope
@kotlin.annotation.Target(AnnotationTarget.TYPE, AnnotationTarget.FUNCTION)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class ViewScope