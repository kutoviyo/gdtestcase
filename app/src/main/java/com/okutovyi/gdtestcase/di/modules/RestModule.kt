package com.okutovyi.gdtestcase.di.modules

import com.google.gson.GsonBuilder
import com.okutovyi.gdtestcase.api.Api
import com.okutovyi.gdtestcase.api.QueryParameterAddInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.JavaNetCookieJar
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.net.CookieManager
import java.net.CookiePolicy
import javax.inject.Singleton

@Module
class RestModule {

    companion object {
        const val BASE_URL = "https://api.openweathermap.org/data/2.5/"
        private const val DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ"
    }

    @Singleton
    @Provides
    fun provideApi(queryParameterAddInterceptor: QueryParameterAddInterceptor): Api {
        val httpLogger = HttpLoggingInterceptor()
        httpLogger.level = HttpLoggingInterceptor.Level.BODY
        val gson = GsonBuilder().setDateFormat(DATE_FORMAT).create()
        val cookieManager = CookieManager()
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL)
        val client = OkHttpClient.Builder()
            .cookieJar(JavaNetCookieJar(cookieManager))
            .addInterceptor(queryParameterAddInterceptor)
            .addInterceptor(httpLogger)
            .build()
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .client(client)
            .build().create(Api::class.java)
    }
}
