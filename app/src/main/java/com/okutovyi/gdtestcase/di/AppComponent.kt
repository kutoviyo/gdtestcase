package com.okutovyi.gdtestcase.di

import com.okutovyi.gdtestcase.GdTestCaseApplication
import com.okutovyi.gdtestcase.di.modules.ActivityBindingModule
import com.okutovyi.gdtestcase.di.modules.AppModule
import com.okutovyi.gdtestcase.di.modules.RepositoryModule
import com.okutovyi.gdtestcase.di.modules.RestModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [(AndroidSupportInjectionModule::class), (ActivityBindingModule::class),
        (AppModule::class), (RestModule::class), (RepositoryModule::class)]
)
interface AppComponent : AndroidInjector<GdTestCaseApplication> {

    override fun inject(instance: GdTestCaseApplication)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: GdTestCaseApplication): Builder

        fun build(): AppComponent
    }
}