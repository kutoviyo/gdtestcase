package com.okutovyi.gdtestcase.di.modules

import com.okutovyi.gdtestcase.api.Api
import com.okutovyi.gdtestcase.repository.ForecastRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Singleton
    @Provides
    fun provideForecastRepository(api: Api) = ForecastRepository(api)

}