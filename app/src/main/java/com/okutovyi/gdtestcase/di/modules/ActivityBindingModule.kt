package com.okutovyi.gdtestcase.di.modules

import com.okutovyi.gdtestcase.di.scopes.ActivityScope
import com.okutovyi.gdtestcase.ui.search.SearchActivity
import com.okutovyi.gdtestcase.ui.search.SearchModule
import com.okutovyi.gdtestcase.ui.weatherdetails.WeatherDetailsActivity
import com.okutovyi.gdtestcase.ui.weatherdetails.WeatherDetailsModule
import com.okutovyi.gdtestcase.ui.weatherlist.WeatherListActivity
import com.okutovyi.gdtestcase.ui.weatherlist.WeatherListModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [(SearchModule::class)])
    abstract fun bindSearchActivity(): SearchActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(WeatherListModule::class)])
    abstract fun bindWeatherListActivity(): WeatherListActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(WeatherDetailsModule::class)])
    abstract fun bindWeatherDetailsActivity(): WeatherDetailsActivity
}