package com.okutovyi.gdtestcase.di.modules

import android.app.Application
import android.content.Context
import android.content.res.Resources
import com.okutovyi.gdtestcase.GdTestCaseApplication
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [(AppModule.Declarations::class)])
class AppModule {

    @Module
    interface Declarations {
        @Binds
        fun application(app: GdTestCaseApplication): Application
    }

    @Singleton
    @Provides
    fun provideContext(app: GdTestCaseApplication): Context = app.applicationContext

    @Singleton
    @Provides
    fun provideResources(app: GdTestCaseApplication): Resources = app.resources
}