package com.okutovyi.gdtestcase.di.qualifier

import javax.inject.Qualifier

@Qualifier
annotation class ApiClient