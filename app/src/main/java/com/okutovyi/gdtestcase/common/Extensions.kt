package com.okutovyi.gdtestcase.common

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

fun ViewGroup.inflate(layoutId: Int): View =
        LayoutInflater.from(context).inflate(layoutId, this, false)

fun Context.launchActivity(activityClass: Class<*>) {
    val intent = Intent(this, activityClass)
    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
    startActivity(intent)
}

fun Context.launchActivity(activityClass: Class<*>, data: Pair<String, Int>) {
    val intent = Intent(this, activityClass)
    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
    intent.putExtra(data.first, data.second)
    startActivity(intent)
}
